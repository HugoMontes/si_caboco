  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.5
    </div>
    <strong>Copyright &copy; 2016 <a href="http://formaempresas.com/" target="_blank">Formaempresas</a>.</strong> Todos los derechos reservados.
  </footer>