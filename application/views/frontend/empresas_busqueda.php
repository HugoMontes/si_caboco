<?php $this->load->view('frontend/template/header'); ?>
<!-- Start Page Banner -->
<div class="page-banner no-subtitle">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h2><?php echo $titulo; ?></h2>
      </div>
    </div>
  </div>
</div>
<!-- End Page Banner -->

    <!-- Start All Noticias -->
    <div class="container container-caboco">
    	<div class="row">
    		<div class="col-md-6">
    			<form>
    				<div class="form-group">
    					<label for="criterio">Criterio</label>
                        <select id="criterio" name="criterio" class="form-control">
                            <option>Registro</option>
                            <option>Sigla</option>
                            <option>Nombre</option>
                        </select>
    				</div>
    				<div class="form-group">
    					<label for="campo_busqueda">Registro</label>
    					<input type="text" class="form-control" id="campo_busqueda" name="campo_busqueda" placeholder="Ingrese el texto para la busqueda">
    				</div>    				
    				<button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                </form>
            </div>
       	</div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <tr>
                        <th>Sigla</th>
                        <th>Nombre empresa</th>
                        <th>Gerente general</th>
                    </tr>
                    <tr>
                        <td>A.A.</td>
                        <td>CONSTRUCTORA AGUA ANDINA S.R.L.</td>
                        <td>ING. JUAN JOSE ORTUÑO VILLAROEL</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- End All Noticias -->
<?php $this->load->view('frontend/template/footer'); ?>
