<div class="navbar navbar-default navbar-top">
	<a class="navbar-brand" href="<?php echo base_url('/');?>">
		<img alt="" style="margin-left: 40px; height: 70px;" src="<?php echo base_url('assets/img/logo/'.$mod_logo->contenido);?>">
	</a>
	<div class="container-fluid" style="height: 85px;">
		<div class="navbar-header">
			<!-- Stat Toggle Nav Link For Mobiles -->
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<i class="fa fa-bars"></i>
			</button>
			<!-- End Toggle Nav Link For Mobiles -->			
		</div>
		<div class="navbar-collapse collapse">
			<!-- Start Navigation List -->
			<ul class="nav navbar-nav navbar-right">
				<?php echo $menus;?>
			</ul>
			<!-- End Navigation List -->
		</div>
	</div>
	
	
	<!-- Mobile Menu Start -->
	<ul class="wpb-mobile-menu">
		<?php echo $menus;?>
	</ul>
	<!-- Mobile Menu End -->
</div>